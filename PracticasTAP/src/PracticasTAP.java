import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Adal
 */
public class PracticasTAP {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         JFrame frm = new JFrame("Ejemplo eventos");
        
        JLabel lbl1 = new JLabel("Escribe un nombre para saludar");
        
        JTextField tfNombre = new JTextField("",20); 
        
        frm.setSize(300,200);
        
        FlowLayout bl = new FlowLayout();
        
        frm.setLayout(bl);
        
        frm.addWindowListener(new WindowAdapter(){
                                 public void windowClosing(WindowEvent e){
                                     System.exit(0);
                                 }
                              });
        
        JButton btn = new JButton("Saludar");
        
        btn.addMouseListener(new MouseAdapter(){
                                   @Override
                                   public void mouseClicked(MouseEvent e){
                                       System.exit(0);
                                   }
                              });
           
        btn.addMouseMotionListener(new MouseMotionAdapter() {
                                        @Override
                                        public void mouseMoved(MouseEvent e) {
                                            int x=0,y=0;

                                            x = e.getX();
                                            y = e.getY();

                                            System.out.println("x="+x+", y="+y);
                                        }
                                     });
        
        frm.add(lbl1);
        frm.add(tfNombre);
        frm.add(btn);
        
        frm.setVisible(true);
    }
}
    

